package ProjLoginLogout.ProjLoginLogout;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import java.io.IOException;
import java.util.List;

import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;
import org.openqa.selenium.WebDriver;
import org.testng.ISuite;
import org.testng.annotations.AfterMethod;


// Edit Comment for testing build trigger updated for new clone
public class LoginLogouttestNG {
	LoginLogout j = new LoginLogout();
	WebDriver driver ;
  @Test(priority =0)
  public void login() throws FileFormatException, IOException
  {		 	j.login();
	  		System.out.println("Testing Login and logout..");
  }
  @BeforeMethod
  public void beforeMethod() {
	 j.TestStartTime();
	  driver = j.chromeBrowser();
	 
	  }
  @AfterMethod
  public void afterMethod() {
	  j.logout();
	  j.TestEndTime();
	 driver.quit();
  }
  public void generateReport(List<ISuite> suites, String outputDirectory){}
}
